import { showWinnerModal } from "./modals/winner";

export function fight(firstFighter, secondFighter) {
  var winner = firstFighter;
  while (firstFighter.health > 0 && secondFighter.health > 0){
    if(secondFighter.health > 0){
      firstFighter.health -= getDamage(firstFighter, secondFighter);
      if (firstFighter.health > 0){
        secondFighter.health -= getDamage(secondFighter, firstFighter);
      } else { firstFighter.health = 0;}
    }
  }

  if(secondFighter.health > 0){
    winner = secondFighter;
  } else { secondFighter.health = 0; }
  return winner;
}

export function getDamage(attacker, enemy) {
  var power = getHitPower(enemy) - getBlockPower(attacker);
  if (power < 0) {power = 0;}
  return power;
}

export function getHitPower(fighter) {
  return fighter.attack * getRandomNumber(1, 2);
}

export function getBlockPower(fighter) {
  return fighter.defense * getRandomNumber(1, 2);
}

function getRandomNumber(min, max) {
  return Math.random() * (max - min) + min;
}
