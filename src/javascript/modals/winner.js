import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { createImage } from  '../fighterView'

export  function showWinnerModal(fighter) {
  // show winner name and image
  const title = 'Winner:';
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });
}

function createWinnerDetails(fighter) {
  const { name, source } = fighter;

  const winnerDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const imgElement = createImage( source );

  nameElement.innerText = name;
  winnerDetails.append(nameElement, imgElement);

  return winnerDetails;
}