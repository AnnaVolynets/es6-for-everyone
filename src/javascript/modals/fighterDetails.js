import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { createImage } from  '../fighterView'

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
  const imgElement = createImage( source );
  // show fighter name, attack, defense, health, image

  nameElement.innerText = name;
  attackElement.innerText = " attack:" + attack;
  defenseElement.innerText = " defense:" + defense;
  healthElement.innerText = " health:" + health;
  fighterDetails.append(nameElement, attackElement, defenseElement, healthElement, imgElement);

  return fighterDetails;
}
